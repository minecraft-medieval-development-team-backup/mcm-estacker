package com.mcm.estacker.listeners;

import com.mcm.core.utils.NBTTagUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EntityDropEvent implements Listener {

    // 30 > 40 > 50
    @EventHandler
    public void onDeath(EntityDeathEvent event) {
        if (event.getEntity().getMetadata("level").size() >= 1) {
            String level = event.getEntity().getMetadata("level").get(0).asString();

            if (level.equals("1")) {
                tryDuply(30, 2, event);
            } else if (level.equals("2")) {
                tryDuply(40, 3, event);
            } else if (level.equals("3")) {
                tryDuply(50, 4, event);
            }
        }
    }

    private static void tryDuply(int porcent, int multiply, EntityDeathEvent event) {
        List<ItemStack> dropsClone = new ArrayList<>();
        if (random(porcent)) {
            int m = new Random().nextInt(multiply);
            if (m >= 2) {
                for (ItemStack item : event.getDrops()) {
                    dropsClone.add(item);
                }
                for (int i = 0; i < m; i++) {
                    event.getDrops().clear();
                    for (ItemStack item : dropsClone) {
                        event.getDrops().add(NBTTagUtil.setData(item, "guild_spawner", ""));
                    }
                }
            }
        }
    }

    private static boolean random(int porcent) {
        int random = new Random().nextInt(100);
        if (random <= porcent) return true; else return false;
    }
}
