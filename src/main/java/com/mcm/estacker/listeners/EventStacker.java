package com.mcm.estacker.listeners;

import com.mcm.estacker.Main;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Arrays;

public class EventStacker implements Listener {

    /**
     * Entities no stackables
     */
    private static EntityType[] noStackable = {EntityType.ARMOR_STAND, EntityType.BOAT, EntityType.DRAGON_FIREBALL, EntityType.DROPPED_ITEM, EntityType.ENDER_CRYSTAL, EntityType.ENDER_PEARL, EntityType.EXPERIENCE_ORB, EntityType.FALLING_BLOCK, EntityType.FIREBALL,
            EntityType.FISHING_HOOK, EntityType.FIREWORK, EntityType.LEASH_HITCH, EntityType.ITEM_FRAME, EntityType.LIGHTNING, EntityType.MINECART, EntityType.MINECART_CHEST, EntityType.MINECART_COMMAND, EntityType.MINECART_FURNACE, EntityType.MINECART_HOPPER,
            EntityType.MINECART_MOB_SPAWNER, EntityType.MINECART_TNT, EntityType.PAINTING, EntityType.PLAYER, EntityType.SMALL_FIREBALL, EntityType.PRIMED_TNT, EntityType.SNOWBALL, EntityType.SPECTRAL_ARROW, EntityType.SPLASH_POTION, EntityType.STRAY,
            EntityType.THROWN_EXP_BOTTLE, EntityType.TRIDENT, EntityType.WITHER_SKULL};

    /**
     * The distance to stack the entities
     */
    private static int distance = 100;

    /**
     * Here i'm corriging the problem if player click with a egg on hand on spawner
     * @param event
     */
    @EventHandler
    public void spawnChange(PlayerInteractEvent event) {
        if (event.getAction().name().equals("RIGHT_CLICK_BLOCK") && event.getPlayer().getItemInHand() != null && event.getPlayer().getItemInHand().getType().name().contains("EGG") && event.getClickedBlock().getType().name().contains("SPAWNER")) event.setCancelled(true);
    }

    /**
     * If the entity deaths, but he has more of two entities stackables him respawn with -1
     * @param event
     */
    @EventHandler
    public void onDeath(EntityDeathEvent event) {
        if (event.getEntity().getMetadata("count").size() >= 1) {
            if (event.getEntity().getMetadata("count").get(0).asInt() > 1) {
                Entity respawn = event.getEntity().getWorld().spawnEntity(event.getEntity().getLocation(), event.getEntityType());
                respawn.setMetadata("count", new FixedMetadataValue(Main.plugin, event.getEntity().getMetadata("count").get(0).asInt() - 1));
                if (event.getEntity().getMetadata("level").size() >= 1) {
                    respawn.setCustomName(ChatColor.GREEN + String.valueOf(event.getEntity().getMetadata("count").get(0).asInt()) + "x  " + ChatColor.RED + "LEVEL-" + event.getEntity().getMetadata("level").get(0).asString());
                } respawn.setCustomName(ChatColor.GREEN + String.valueOf(event.getEntity().getMetadata("count").get(0).asInt()) + "x");
                respawn.setCustomNameVisible(true);
            }
        }
    }

    /**
     * Checking if the entity's spawning on spawner, and if the spawner has level i apply on entity the level of the same
     * @param event
     */
    @EventHandler (priority = EventPriority.LOWEST)
    public void onSpawner(SpawnerSpawnEvent event) {
        Block spawner = event.getSpawner().getBlock();
        if (com.mcm.core.utils.NBTTagUtil.getStringData(spawner, "level") != null) {
            event.getEntity().setMetadata("level", new FixedMetadataValue(Main.plugin, com.mcm.core.utils.NBTTagUtil.getStringData(spawner, "level")));
        }
    }

    /**
     * Stacking the entities on 'distance' specificated
     * @param event
     */
    @EventHandler (priority = EventPriority.MONITOR)
    public void onSpawn(EntitySpawnEvent event) {
        if (Arrays.asList(noStackable).contains(event.getEntityType())) return;

        boolean have = false;
        for (Entity target : event.getEntity().getWorld().getNearbyEntities(event.getLocation(), distance, distance, distance)) {
            if (target.getType().equals(event.getEntity().getType())) {
                if (target.getMetadata("level").size() >= 1 && event.getEntity().getMetadata("level").size() >= 1 && !target.getMetadata("level").get(0).asString().equals(event.getEntity().getMetadata("level").get(0).asString())) return;

                event.setCancelled(true);
                target.setMetadata("count", new FixedMetadataValue(Main.plugin, target.getMetadata("count").get(0).asInt() + 1));
                if (target.getMetadata("level").size() >= 1) {
                    target.setCustomName(ChatColor.GREEN + String.valueOf(target.getMetadata("count").get(0).asInt()) + "x  " + ChatColor.RED + "LEVEL-" + target.getMetadata("level").get(0).asString());
                } target.setCustomName(ChatColor.GREEN + String.valueOf(target.getMetadata("count").get(0).asInt()) + "x");
                target.setCustomNameVisible(true);
                have = true;
                break;
            }
        }

        if (!have) {
            event.getEntity().setMetadata("count", new FixedMetadataValue(Main.plugin, 1));
            if (event.getEntity().getMetadata("level").size() >= 1) {
                event.getEntity().setCustomName(ChatColor.GREEN + String.valueOf(event.getEntity().getMetadata("count").get(0).asInt()) + "x  " + ChatColor.RED + "LEVEL-" + event.getEntity().getMetadata("level").get(0).asString());
            } event.getEntity().setCustomName(ChatColor.GREEN + String.valueOf(event.getEntity().getMetadata("count").get(0).asInt()) + "x");
            event.getEntity().setCustomNameVisible(true);
        }
    }
}
