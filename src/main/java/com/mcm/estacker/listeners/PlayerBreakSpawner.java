package com.mcm.estacker.listeners;

import com.mcm.core.utils.NBTTagUtil;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class PlayerBreakSpawner implements Listener {

    private static Material[] pickaxes = {Material.DIAMOND_PICKAXE, Material.GOLDEN_PICKAXE, Material.IRON_PICKAXE};

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();

        if (event.getBlock().getType().name().equals("SPAWNER") && player.getItemInHand() != null && Arrays.asList(pickaxes).contains(player.getItemInHand().getType())) {
            event.setCancelled(true);

            BlockState blockState = event.getBlock().getState();
            CreatureSpawner spawner = (CreatureSpawner) blockState;

            ItemStack drop = new ItemStack(Material.SPAWNER);
            drop.getItemMeta().setDisplayName("Spawner " + spawner.getSpawnedType().name().replaceAll("_", " "));
            NBTTagUtil.setData(drop, "type", spawner.getSpawnedType().name());

            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), drop);
            event.getBlock().setType(Material.AIR);
        }
    }
}
