package com.mcm.estacker.listeners;

import com.mcm.core.utils.NBTTagUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerPlaceSpawner implements Listener {

    @EventHandler
    public void onPlace(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (event.getAction().name().equals("RIGHT_CLICK_BLOCK") && player.getInventory().getItemInMainHand().getType().equals(Material.SPAWNER)) {
            event.setCancelled(true);

            Block block = event.getClickedBlock().getRelative(event.getBlockFace());
            block.setType(Material.SPAWNER);
            player.playSound(block.getLocation(), block.getSoundGroup().getPlaceSound(), 1.0f, 1.0f);

            BlockState blockState = block.getState();
            CreatureSpawner spawner = (CreatureSpawner) blockState;
            spawner.setSpawnedType(EntityType.valueOf(NBTTagUtil.getStringData(player.getInventory().getItemInMainHand(), "type")));
            spawner.update();

            if (NBTTagUtil.getStringData(player.getInventory().getItemInMainHand(), "level") != null) {
                NBTTagUtil.setData(block, "level", NBTTagUtil.getStringData(player.getInventory().getItemInMainHand(), "level"));
            }

            player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
            player.updateInventory();
        }
    }
}
