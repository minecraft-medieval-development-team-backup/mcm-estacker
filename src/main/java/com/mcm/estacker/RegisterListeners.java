package com.mcm.estacker;

import com.mcm.estacker.listeners.EventStacker;
import com.mcm.estacker.listeners.PlayerBreakSpawner;
import com.mcm.estacker.listeners.PlayerPlaceSpawner;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new EventStacker(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerPlaceSpawner(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerBreakSpawner(), Main.plugin);
    }
}
